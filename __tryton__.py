# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'German Tax Data for DATEV',
    'name_de_DE': 'Deutsche Steuerdaten für DATEV',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''Financial and accounting module (only for Germany):
    -  Provides tax keys and functions for DATEV
''',
    'description_de_DE': '''Buchhaltungsmodul (für Deutschland):
    - Stellt die DATEV-Schlüssel und Funktionen bereit
''',
    'depends': [
        'account_timeline_tax_de',
        'account_timeline_datev'
    ],
    'xml': [
        'account_timeline_tax_de_datev.xml'
    ],
    'translation': [
    ],
}
